const express = require("express");
const app = express();
const ptnp = require("./routes/ptnp");

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/ptnp", ptnp);

const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`Running on port ${port}...`);
});
