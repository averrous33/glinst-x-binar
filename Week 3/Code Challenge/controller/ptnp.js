const data = require("../model/data.json");

class Ptnp {
  listPtnp(req, res, next) {
    try {
      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ error: "Internal Server Error" });
    }
  }

  getPtnpDetail(req, res, next) {
    try {
      const filteredById = data.filter(
        (item) => item.id === parseInt(req.params.id)
      );

      if (filteredById.length === 0) {
        return res.status(404).json({ error: "PTNP Not Found!" });
      }

      res.status(200).json({ data: filteredById });
    } catch (error) {
      res.status(500).json({ error: "Internal Server Error" });
    }
  }

  addPtnp(req, res, next) {
    try {
      let addId = data[data.length - 1].id;

      data.push({
        id: addId + 1,
        ptnp: req.body.ptnp,
        location: req.body.location,
        type: req.body.type,
      });

      res.status(201).json({ data });
    } catch (error) {
      res.status(500).json({ error: "Internal Server Error" });
    }
  }

  updatePtnp(req, res, next) {
    try {
      const findDataUpdate = data.some(
        (item) => item.id === parseInt(req.params.id)
      );

      if (!findDataUpdate) {
        return res.status(404).json({ error: "PTNP Not Found!" });
      }

      const editPtnp = data.map((item) =>
        item.id === parseInt(req.params.id)
          ? {
              id: parseInt(req.params.id),
              ptnp: req.body.ptnp,
              location: req.body.location,
              type: req.body.type,
            }
          : item
      );

      res.status(200).json({ data: editPtnp });
    } catch (error) {
      res.status(500).json({ error: "Internal Server Error" });
    }
  }

  deletePtnp(req, res, next) {
    try {
      const findDataDelete = data.some(
        (item) => item.id === parseInt(req.params.id)
      );

      if (!findDataDelete) {
        return res.status(404).json({ error: "PTNP Not Found!" });
      }

      const deletePtnp = data.filter(
        (item) => item.id !== parseInt(req.params.id)
      );

      res.status(200).json({ data: deletePtnp });
    } catch (error) {
      res.status(500).json({ error: "Internal Server Error" });
    }
  }
}

module.exports = new Ptnp();
