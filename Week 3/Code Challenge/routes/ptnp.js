const express = require("express");
const {
  listPtnp,
  getPtnpDetail,
  addPtnp,
  updatePtnp,
  deletePtnp,
} = require("../controller/ptnp");

const router = express.Router();

router.get("/", listPtnp);
router.get("/:id", getPtnpDetail);
router.post("/", addPtnp);
router.put("/:id", updatePtnp);
router.delete("/:id", deletePtnp);

module.exports = router;
