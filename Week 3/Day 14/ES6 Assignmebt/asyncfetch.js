const fetch = require(`node-fetch`);

const getApi = async () => {
    let urlPage = "https://reqres.in/api/users?page=2";
    let urlLogin = "https://reqres.in/api/login";
    let urlUser = "https://reqres.in/api/users";
    let data = {};
    try {
        const responsePage = await fetch(urlPage);
        const responseLogin = await fetch(urlLogin);
        const responseUser = await fetch(urlUser);
    const response = await Promise.all([
        responsePage.json(),
        responseLogin.json(),
        responseUser.json(),
    ])
    data = {
        page:response[0],
        login:response[1],
        user: response[2],
    }
    console.log(data);
    } catch (error) {
        console.log(error.message);
    }
}
getApi();
