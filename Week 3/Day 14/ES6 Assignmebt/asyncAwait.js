const axios = require(`axios`);

const getApi = async () => {
    let urlPage = "https://reqres.in/api/users?page=2";
    let urlLogin = "https://reqres.in/api/login";
    let urlUser = "https://reqres.in/api/users";
    let data = {};
try { 
    const response = await Promise.all([
      axios.get(urlPage),
      axios.get(urlLogin),
      axios.get(urlUser),
    ]);
    data = {
        page:response[0].data,
        login:response[1].data,
        user: response[2].data,
    }
    console.log(data);
} catch (error) {
    console.log(error.message);
}
}
getApi();