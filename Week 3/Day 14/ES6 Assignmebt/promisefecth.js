const fetch = require(`node-fetch`);
let urlPage = "https://reqres.in/api/users?page=2";
let urlLogin = "https://reqres.in/api/login";
let urlUser = "https://reqres.in/api/users";

let data = {};
fetch (urlPage)
.then ((page) => page.json())
.then ((pageJson)=> {
    data = {page:pageJson}
    return fetch (urlLogin)
})
  .then((urlLogin) => urlLogin.json())
  .then((loginJson) => {
    data = {...data,login: loginJson};
    return fetch(urlUser);
  })
  .then ((urlUser) => urlUser.json())
  .then((userJson) => {
    data = {...data, user: userJson};
    console.log(data);
  })
.catch ((err)=>{
    console.log(err.message);
})


