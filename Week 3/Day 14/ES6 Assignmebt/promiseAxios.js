const axios = require (`axios`)
 let urlPage = "https://reqres.in/api/users?page=2";
 let urlLogin = "https://reqres.in/api/login";
 let urlUser = "https://reqres.in/api/users";
 let data = {}
 axios
   .get(urlPage)
   .then((response) => {
     data = {page:response.data}
     return axios.get(urlLogin);
   })
   .then((response) => {
     data = { ...data, login: response.data };
     return axios.get(urlUser);
   })
   .then((response) => {
     data = { ...data, user: response.data };
     console.log(data);
   })
   .catch((err) => {
     console.log(err.message);
   });