const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

function cone(r, t) {
  console.log((1 / 3) * r * r * t);
  return (1 / 3) * r * r * t;
}

function input() {
  rl.question("r: ", function (r) {
    rl.question("t: ", (t) => {
      if (r > 0 && t > 0) {
        console.log(`\nCOne: ${cone(r, t)}`);
        rl.close();
      } else {
        console.log(`r and t must be a number\n`);
        input();
      }
    });
  });
}

input();


