const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

function triangel(alas, tinggi) {
  console.log((1 / 2) * alas* tinggi);
  return (1 / 2) * alas * tinggi;
}

function input() {
  rl.question("alas: ", function (alas) {
    rl.question("tinggi: ", (tinggi) => {
      if (alas > 0 && tinggi > 0) {
        console.log(`\ntriangel: ${triangel(alas, tinggi)}`);
        rl.close();
      } else {
        console.log(`alas and tinggi must be a number\n`);
        input();
      }
    });
  });
}

input();