const EventEmitter = require("events"); // Import event
const readline = require("readline"); // Import readline
const testFunction = require("../DAY 9/assignment2")

// Make event instance
const my = new EventEmitter();
// Make readline
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

// Listener
my.on("login:failed", function (email) {
  console.log(`${email} is failed to login`);
  rl.close();
});

my.on("login:success", function (email) {
  console.log(`${email} is success to login!`);
  testFunction("Negative");
  testFunction("Positive");
  testFunction("Suspect");
  rl.close();
});

// Function to login
function login(email, password) {
  const passwordLogin = "141224";

  if (password !== passwordLogin) {
    my.emit("login:failed", email); // Pass the email to the listener
  } else {
    // Do something
    my.emit("login:success", email);
  }
}

// Input email and password
rl.question("Email: ", function (email) {
  rl.question("Password: ", function (password) {
    login(email, password); // Run login function
  });
});