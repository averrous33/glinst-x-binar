const phi = 3.14;
function globe(r) {
  console.log((4 / 3) * phi * r * r * r);
  return (4 / 3) * phi * r * r * r;
}

let globeOne = globe(5);
let globeTwo = globe(7);
console.log(globeOne + globeTwo);

console.log(
  "------------------------------------------------------------------------------------"
);

function cone(r, t) {
  console.log((1 / 3) * r * r * t);
  return (1 / 3) * r * r * t;
}

let coneOne = cone(7, 9);
let coneTwo = cone(5, 11);
console.log(coneOne + coneTwo);
